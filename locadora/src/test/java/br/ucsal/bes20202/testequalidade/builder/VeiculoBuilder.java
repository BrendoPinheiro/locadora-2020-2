package br.ucsal.bes20202.testequalidade.builder;

import br.ucsal.bes20202.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20202.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20202.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuilder {
	public static final String DEFAULT_PLACA = "SKT-T138";
    public static final Integer DEFAULT_ANO = 2012;
    public static final Modelo DEFAULT_MODELO = new Modelo("1.0");
    public static final Double DEFAULT_VALORDIARIA = 550.00;
    public static final SituacaoVeiculoEnum DEFAULT_SITUACAO = SituacaoVeiculoEnum.DISPONIVEL;
	
	private String placa = DEFAULT_PLACA;
	private Integer ano = DEFAULT_ANO;
	private Modelo modelo = DEFAULT_MODELO;
	private Double valorDiaria = DEFAULT_VALORDIARIA;
	private SituacaoVeiculoEnum situacao = DEFAULT_SITUACAO;
	
	private VeiculoBuilder(){}
	
	public static VeiculoBuilder veiculo() {
		return new VeiculoBuilder().withAno(DEFAULT_ANO)
								   .withModelo(DEFAULT_MODELO)
								   .withPlaca(DEFAULT_PLACA)
								   .withSituacao(DEFAULT_SITUACAO)
								   .withValorDiaria(DEFAULT_VALORDIARIA);
	}
	
	public static VeiculoBuilder veiculoDefault() {
        return VeiculoBuilder.veiculo();
    }
	public VeiculoBuilder withPlaca(String placa) {
		this.placa = placa;
		return this;
	}
	public VeiculoBuilder withAno(Integer ano) {
		this.ano = ano;
		return this;
	}
	public VeiculoBuilder withModelo(Modelo modelo) {
		this.modelo = modelo;
		return this;
	}
	public VeiculoBuilder withValorDiaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
		return this;
	}
	public VeiculoBuilder withSituacao (SituacaoVeiculoEnum situacao) {
		this.situacao = situacao;
		return this;
	}
	public VeiculoBuilder but(){
        return VeiculoBuilder.veiculo().withAno(this.ano)
				   .withModelo(this.modelo)
				   .withPlaca(this.placa)
				   .withSituacao(this.situacao)
				   .withValorDiaria(this.valorDiaria);
    }
	public Veiculo build() {
        Veiculo instance = new Veiculo();
        instance.setAnoFabricacao(this.ano);
        instance.setModelo(this.modelo);
        instance.setPlaca(this.placa);
        instance.setSituacao(this.situacao);
        instance.setValorDiaria(this.valorDiaria);
        return instance;
    }

}
