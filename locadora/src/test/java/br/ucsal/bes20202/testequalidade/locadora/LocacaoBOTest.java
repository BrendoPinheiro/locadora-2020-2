package br.ucsal.bes20202.testequalidade.locadora;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import org.mockito.Mockito;

import br.ucsal.bes20202.testequalidade.builder.VeiculoBuilder;
import br.ucsal.bes20202.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20202.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20202.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20202.testequalidade.locadora.persistence.VeiculoDAO;

/**
 * Testes UNITÁRIOS para os métodos da classe LocacaoBO (utilize o MOCKITO).
 * 
 * @author claudioneiva
 *
 */
public class LocacaoBOTest {

	/**
	 * Testar o cálculo do valor de locação por 3 dias para 2 veículos com 1 ano de
	 * fabricaçao e 3 veículos com 8 anos de fabricação.
	 */
	
	private static ArrayList<String> placas;
	private static List<Veiculo> veiculos;
	private static VeiculoDAO veiculoDAOFake;
	private static LocacaoBO locacaoBO;
	
	@BeforeAll
	public static void setup() throws VeiculoNaoEncontradoException {
		placas = new ArrayList<String>();
		VeiculoBuilder builder = VeiculoBuilder.veiculo();
		Veiculo Kawasaki1 = builder.but().build();
		Veiculo Kawasaki2 = builder.but().withPlaca("SAK-URA1").withAno(2020).withValorDiaria(200.0).build();
		Veiculo Kawasaki3 = builder.but().withPlaca("NAR-UTO2").withAno(2013).withValorDiaria(150.0).build();
		Veiculo Kawasaki4 = builder.but().withPlaca("SAS-UKE3").withAno(2015).withValorDiaria(400.0).build();
		Veiculo Kawasaki5 = builder.but().withPlaca("ITA-CHI4").withAno(2019).withValorDiaria(160.0).build();
		
		veiculos = Arrays.asList(Kawasaki1, Kawasaki2, Kawasaki3, Kawasaki4, Kawasaki5);
		
		placas.add("ZAB-UZA1");
	    placas.add("ROC-KLEE");
	    placas.add("JIR-AIA3");
	    placas.add("NAG-ATO4");
	    placas.add("MAD-ARA5");
	    
	    veiculoDAOFake = Mockito.mock(VeiculoDAO.class);
		
		locacaoBO = new LocacaoBO(veiculoDAOFake);
		Mockito.when(veiculoDAOFake.obterPorPlacas(Mockito.anyList())).thenReturn(veiculos);
	}

	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() throws VeiculoNaoEncontradoException {
		double valorEsperado = 4170.0;
		assertEquals(valorEsperado , locacaoBO.calcularValorTotalLocacao(placas, 3, LocalDate.now()));
	}
	
}
