package br.ucsal.bes20202.testequalidade.builder;

import br.ucsal.bes20202.testequalidade.locadora.dominio.Cliente;

public class ClienteBuilder {
	public static final String DEFAULT_NOME = "Darth Vader";
    public static final String DEFAULT_CPF = "666.666.124-24";
    public static final String DEFAULT_TELEFONE = "71999999999";
	
    private String nome = DEFAULT_NOME;
	private String cpf = DEFAULT_CPF;
	private String telefone = DEFAULT_TELEFONE;
	
	private ClienteBuilder(){}
	
	public static ClienteBuilder cliente() {
	        return new ClienteBuilder();
	    }
	public static ClienteBuilder clienteDefault() {
        return ClienteBuilder.cliente()
            .withNome(DEFAULT_NOME)
            .withCPF(DEFAULT_CPF)
            .withTelefone(DEFAULT_TELEFONE);
    }
	
	public ClienteBuilder withNome(String nome) {
		this.nome = nome;
		return this;
	}
	public ClienteBuilder withCPF(String cpf) {
		this.cpf = cpf;
		return this;
	}
	public ClienteBuilder withTelefone(String telefone) {
		this.telefone = telefone;
		return this;
	}
	public ClienteBuilder but(){
        return ClienteBuilder.cliente().withCPF(this.cpf).withNome(this.nome).withTelefone(this.telefone);
    }
	public Cliente build() {
        Cliente instance = new Cliente(this.cpf,this.nome,this.telefone);
        return instance;
    }

}
