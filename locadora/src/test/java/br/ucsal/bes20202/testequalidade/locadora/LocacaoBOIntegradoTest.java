package br.ucsal.bes20202.testequalidade.locadora;

/**
 * Testes INTEGRADOS para os métodos da classe LocacaoBO.
 * 
 * @author claudioneiva
 *
 */

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20202.testequalidade.builder.ClienteBuilder;
import br.ucsal.bes20202.testequalidade.builder.VeiculoBuilder;
import br.ucsal.bes20202.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20202.testequalidade.locadora.dominio.Cliente;
import br.ucsal.bes20202.testequalidade.locadora.dominio.Locacao;
import br.ucsal.bes20202.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20202.testequalidade.locadora.exception.CampoObrigatorioNaoInformado;
import br.ucsal.bes20202.testequalidade.locadora.exception.ClienteNaoEncontradoException;
import br.ucsal.bes20202.testequalidade.locadora.exception.LocacaoNaoEncontradaException;
import br.ucsal.bes20202.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20202.testequalidade.locadora.persistence.ClienteDAO;
import br.ucsal.bes20202.testequalidade.locadora.persistence.LocacaoDAO;
import br.ucsal.bes20202.testequalidade.locadora.persistence.VeiculoDAO;
import br.ucsal.bes20202.testequalidade.locadora.util.ValidadorUtil;

public class LocacaoBOIntegradoTest {

	/**
	 * Testar o cálculo do valor de locação por 3 dias para 2 veículos com 1 ano de
	 * fabricaçao e 3 veículos com 8 anos de fabricação.
	 */

	public static ClienteBuilder builderCliente;
	public static VeiculoBuilder builderVeiculo;

	private VeiculoDAO veiculoDAO = new VeiculoDAO();
	private LocacaoBO locacaoBO = new LocacaoBO(veiculoDAO);

	@BeforeAll
	public static void setup() {
		builderCliente = ClienteBuilder.clienteDefault();
		builderVeiculo = VeiculoBuilder.veiculoDefault();
	}

	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias()
			throws ClienteNaoEncontradoException, VeiculoNaoEncontradoException, LocacaoNaoEncontradaException {
		Cliente cliente = builderCliente.but()
				.withCPF("999.999.999-99")
				.withNome("Brendo Souza Pinheiro")
				.withTelefone("71912345678")
				.build();
		ClienteDAO.insert(cliente);
		cliente = ClienteDAO.obterPorCpf(cliente.getCpf());
		
		Veiculo Kawasaki1 = builderVeiculo.but().build();
		Veiculo Kawasaki2 = builderVeiculo.but().withPlaca("SAK-URA1").withAno(2020).withValorDiaria(200.0).build();
		Veiculo Kawasaki3 = builderVeiculo.but().withPlaca("NAR-UTO2").withAno(2013).withValorDiaria(150.0).build();
		Veiculo Kawasaki4 = builderVeiculo.but().withPlaca("SAS-UKE3").withAno(2015).withValorDiaria(400.0).build();
		Veiculo Kawasaki5 = builderVeiculo.but().withPlaca("ITA-CHI4").withAno(2019).withValorDiaria(160.0).build();
		
		veiculoDAO.insert(Kawasaki1);
		veiculoDAO.insert(Kawasaki2);
		veiculoDAO.insert(Kawasaki3);
		veiculoDAO.insert(Kawasaki4);
		veiculoDAO.insert(Kawasaki5);
		
		List<Veiculo> veiculos = new ArrayList<>();
		veiculos.add(veiculoDAO.obterPorPlaca(Kawasaki1.getPlaca()));
		veiculos.add(veiculoDAO.obterPorPlaca(Kawasaki2.getPlaca()));
		veiculos.add(veiculoDAO.obterPorPlaca(Kawasaki3.getPlaca()));
		veiculos.add(veiculoDAO.obterPorPlaca(Kawasaki4.getPlaca()));
		veiculos.add(veiculoDAO.obterPorPlaca(Kawasaki5.getPlaca()));
		
		Locacao locacao = new Locacao(cliente, veiculos, new Date(), 3);
		LocacaoDAO.insert(locacao);
		locacao = LocacaoDAO.obterPorNumeroContrato(locacao.getNumeroContrato());

		List<String> placas = new ArrayList<String>();
		placas.add(Kawasaki1.getPlaca());
		placas.add(Kawasaki2.getPlaca());
		placas.add(Kawasaki3.getPlaca());
		placas.add(Kawasaki4.getPlaca());
		placas.add(Kawasaki5.getPlaca());

		Double totalEsperado = 4170.0;
		Double totalAtual = locacaoBO.calcularValorTotalLocacao(placas, locacao.getQuantidadeDiasLocacao(),
				locacao.getDataLocacao().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
		assertEquals(totalEsperado, totalAtual);
	}

	@Test
	public void testarExceptions() {
		assertThrows(CampoObrigatorioNaoInformado.class, () -> {
			Integer numero = null;
			ValidadorUtil.validarCampoObrigatorio(numero, "numero");
		});

		assertThrows(VeiculoNaoEncontradoException.class, () -> {
			veiculoDAO.obterPorPlaca("PAI-DEAD");
		});

		assertThrows(LocacaoNaoEncontradaException.class, () -> {
			LocacaoDAO.obterPorNumeroContrato(50);
		});

		assertThrows(CampoObrigatorioNaoInformado.class, () -> {
			List lista = null;
			ValidadorUtil.validarCampoObrigatorio(lista, "lista");
		});

		assertThrows(CampoObrigatorioNaoInformado.class, () -> {
			Date data = null;
			ValidadorUtil.validarCampoObrigatorio(data, "data");
		});
		
		assertThrows(ClienteNaoEncontradoException.class, () -> {
			ClienteDAO.obterPorCpf("6924");
		});
	}

}
